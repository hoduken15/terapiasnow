@extends('adminlte::page')

@section('title', 'Busqueda')

@section('content')
<form method="GET" action="{{ route('search')}}">	@csrf
	<div class="input-group">
		<input type="text" name="buscar" placeholder="Buscar ..."
			class="form-control"> <span class="input-group-btn"> <a
			class="fa fa-search"><button type="button"
					class="btn btn-search btn-flat"></a>
		</span>
	</div>
	<br> 
	@if($busquedaTerapia) 
    	@foreach($busquedaTerapia as $terapia)
    	{{ $terapia->nombre }}
    	<ul>
    		@if($busquedaTerapeuta)
				@foreach($busquedaTerapeuta as $terapeuta)
				<li>{{ $terapeuta->nombre }} {{ $terapeuta->email }}</li>
				@endforeach 
			@endif
		</ul>
    	@endforeach 
	@endif 
</form>

@endsection