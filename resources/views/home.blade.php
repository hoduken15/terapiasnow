@extends('adminlte::page')

@section('title', 'Home')

@section('content')


<table style="width: 100%;">
	<tr>
		<th>Titulo</th>
		<th>Descripción</th>
		<th>Fecha</th>
	</tr>
	@foreach($noticia as $noticiaItem)
		<tr>
			<td>{{$noticiaItem->titulo}}</td>
			<td>{{$noticiaItem->detalle}}</td>
			<td>{{$noticiaItem->fecha}}</td>
		</tr>
	@endforeach
</table>


@endsection