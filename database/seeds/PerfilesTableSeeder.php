<?php

use Illuminate\Database\Seeder;

class PerfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('perfiles')->insert(['perfil' => 'Cliente']);
        DB::table('perfiles')->insert(['perfil' => 'Terapeuta']);
        DB::table('perfiles')->insert(['perfil' => 'Administrador']);
    }
}
