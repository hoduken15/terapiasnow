<?php

use Illuminate\Database\Seeder;

class NoticiasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=0; $i<=100; $i++):
            DB::table('noticias')
                ->insert([
                    'titulo' => $faker->word,
                    'detalle' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                    'fecha' => $faker->date($format = 'Y-m-d', $max = 'now')
                ]);
        endfor;
    }
}
