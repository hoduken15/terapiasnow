<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=0; $i<=100; $i++):
            DB::table('Users')
                ->insert([
                    'name' => $faker->name,
                    'email' => $faker->email,
                    'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        			'remember_token' => Str::random(10),
                    'rut' => Rut::set(rand(1000000, 25000000))->fix()->format(),
                ]);
        endfor;
    }
}
