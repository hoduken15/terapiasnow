<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('home', 'HomeController@index')->name('home');
Route::get('perfil', 'PerfilController@index')->name('perfil');
Route::get('notificaciones', 'NotificacionesController@index')->name('notificaciones');


Route::get('search','SearchController@index')->name('search');
// Route::get('buscar','SearchController@show')->name('buscar');

Route::get('crearterapia',function(){
	return view('CrearTerapia');
})->name('CrearTerapia');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
