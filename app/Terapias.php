<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terapias extends Model
{
    public function scopeName($query, $name)
    {
    	if ($name) {
    		return $query->where('nombre','LIKE',"%$name%");
    	}
    }

    public function Terapias()
    {
    	return $this->belongsToMany('App\Terapeutas');
    }
}
