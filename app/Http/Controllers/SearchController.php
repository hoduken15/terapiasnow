<?php

namespace App\Http\Controllers;

use App\Terapeutas;
use App\Terapias;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $busquedaTerapia = null;
        $terapeutas = null;
        if ($request->get('buscar')!="") {
            $palabra = $request->get('buscar');
            $busquedaTerapia = Terapias::Name($palabra)->limit(1)->get();
            
            if ($busquedaTerapia->isEmpty()) {
                return view('search', [
                'busquedaTerapia' => null
            ], [
                'busquedaTerapeuta' => null
            ]);
            }

            $TerapiaTerapeuta = DB::table('terapia_terapeutas')->where('id_terapia', $busquedaTerapia[0]->id)->limit(10)->get();

            $id_terapeutas = $TerapiaTerapeuta->pluck('id_user');

            $terapeutas = User::whereIn('id',$id_terapeutas)->get();
            
            if ($terapeutas->isEmpty()) {
                return view('search', [
                'busquedaTerapia' => null
            ], [
                'busquedaTerapeuta' => null
            ]);
            }

            return view('search', [
                'busquedaTerapia' => $busquedaTerapia
            ], [
                'busquedaTerapeuta' => $terapeutas
            ]);
        } else {
            return view('search', [
                'busquedaTerapia' => $busquedaTerapia
            ], [
                'busquedaTerapeuta' => $terapeutas
            ]);}
    }

}
